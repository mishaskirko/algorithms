include .env

up: # create and start containers
	@docker compose -f ${DOCKER_CONFIG_FILE} up -d

down: # stop and destroy containers
	@docker compose -f ${DOCKER_CONFIG_FILE} down

downVolume: #  WARNING: stop and destroy containers with volumes
	@docker compose -f ${DOCKER_CONFIG_FILE} down -v

start: # start already created containers
	@docker compose -f ${DOCKER_CONFIG_FILE} start

stop: # stop containers, but not destroy
	@docker compose -f ${DOCKER_CONFIG_FILE} stop

ps: # show started containers and their status
	@docker compose -f ${DOCKER_CONFIG_FILE} ps

build: # build all dockerfile, if not built yet
	@docker compose -f ${DOCKER_CONFIG_FILE} build

connectApp: # app shell
	@docker compose -f ${DOCKER_CONFIG_FILE} exec -u www -w /www/app/Tasks app sh

connectAppRoot: # app shell
	@docker compose -f ${DOCKER_CONFIG_FILE} exec -u www -w /www/ app sh