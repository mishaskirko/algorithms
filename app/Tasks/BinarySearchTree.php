<?php
declare(strict_types=1);

class Node
{
    /**
     * @param int|string|null $data
     * @param ?Node $left
     * @param ?Node $right
     */
    public function __construct(
        public null|int|string $data,
        public ?Node $left = null,
        public ?Node $right = null
    ) {}
}

class BinarySearchTree
{
    /**
     * @param ?Node $root
     */
    public function __construct(
        public ?Node $root = null
    ) {}

    /**
     * @param $data
     * @return void
     */
    public function insert($data): void
    {
        $newNode = new Node($data);
        if ($this->root == null) {
            $this->root = $newNode;
        } else {
            $this->insertNode($this->root, $newNode);
        }
    }

    /**
     * @param $node
     * @param $newNode
     * @return void
     */
    public function insertNode($node, $newNode): void
    {
        if ($newNode->data < $node->data) {
            if ($node->left == null) {
                $node->left = $newNode;
            } else {
                $this->insertNode($node->left, $newNode);
            }
        } else {
            if ($node->right == null) {
                $node->right = $newNode;
            } else {
                $this->insertNode($node->right, $newNode);
            }
        }
    }

    /**
     * @param $node
     * @param $space
     * @return void
     */
    public function printTree($node, $space): void
    {
        if ($node === null) {
            return;
        }
        $this->printTree($node->right, $space + 10);
        echo "<br/><br/><br/>";
        for ($i = 0; $i < $space; $i++) {
            echo "&nbsp;&nbsp;&nbsp;";
        }
        echo $node->data;
        $this->printTree($node->left, $space + 10);
    }

    /**
     * @param BinarySearchTree $tree
     * @param int $target
     * @return int|null
     */
    public static function findClosestVal(BinarySearchTree $tree, int $target): ?int
    {
        $closest = INF;
        $currentNode = $tree->root;

        while ($currentNode !== null) {
            if (abs($target - $closest) > abs($target - $currentNode->data)) {
                $closest = $currentNode->data;
            }
            if ($target < $currentNode->data) {
                $currentNode = $currentNode->left;
            } elseif ($target > $currentNode->data) {
                $currentNode = $currentNode->right;
            } else {
                break;
            }
        }
        return $closest;
    }

    public function balanceTree(): void
    {
        $this->root = $this->balanceTreeNode($this->root);
    }

    public function balanceTreeNode(?Node $node): ?Node
    {
        if ($node == null) {
            return null;
        }
        $node->left = $this->balanceTreeNode($node->left);
        $node->right = $this->balanceTreeNode($node->right);
        $balance = $this->getBalance($node);
        if ($balance > 1) {
            if ($this->getBalance($node->left) < 0) {
                $node->left = $this->rotateLeft($node->left);
            }
            return $this->rotateRight($node);
        } elseif ($balance < -1) {
            if ($this->getBalance($node->right) > 0) {
                $node->right = $this->rotateRight($node->right);
            }
            return $this->rotateLeft($node);
        }
        return $node;
    }

    public function rotateLeft(?Node $node): ?Node
    {
        $right = $node->right;
        $node->right = $right->left;
        $right->left = $node;
        return $right;
    }

    public function rotateRight(?Node $node): ?Node
    {
        $left = $node->left;
        $node->left = $left->right;
        $left->right = $node;
        return $left;
    }

    public function getBalance(?Node $node): int
    {
        if ($node == null) {
            return 0;
        }
        return $this->getHeight($node->left) - $this->getHeight($node->right);
    }

    public function getHeight(?Node $node): int
    {
        if ($node == null) {
            return 0;
        }
        return 1 + max($this->getHeight($node->left), $this->getHeight($node->right));
    }

    /**
     * @param ?Node $root
     * @return array
     */
    public function branchSums(?Node $root): array
    {
        $sums = [];
        $this->calculateBranchSums($root, 0, $sums);
        return $sums;
    }

    /**
     * @param ?Node $node
     * @param int $runningSum
     * @param array $sums
     * @return void
     */
    public function calculateBranchSums(?Node $node, int $runningSum, array &$sums): void
    {
        if ($node === null) {
            return;
        }
        $newRunningSum = $runningSum + $node->data;
        if ($node->left === null && $node->right === null) {
            $sums[] = $newRunningSum;
            return;
        }
        $this->calculateBranchSums($node->left, $newRunningSum, $sums);
        $this->calculateBranchSums($node->right, $newRunningSum, $sums);
    }

    /**
     * @param Node $root
     * @return int
     */
    public function nodeDepthSumFirstSolution(Node $root): int
    {
        $sumOfDepth = 0;
        $stack = new Stack();
        $stack->push(new StackItem($root, 0));
        while ($stack->count() > 0) {
            $nodeInfo = $stack->pop();
            $node = $nodeInfo->node;
            $depth = $nodeInfo->depth;
            if ($node === null) {
                continue;
            }
            $sumOfDepth += $depth;
            $stack->push(new StackItem($node->left, $depth + 1));
            $stack->push(new StackItem($node->right, $depth + 1));
        }
        return $sumOfDepth;
    }

    /**
     * @param ?Node $root
     * @param int $depth
     * @return int
     */
    public function nodeDepthSumSecondSolution(?Node $root, int $depth = 0): int
    {
        if ($root === null) {
            return 0;
        }
        return $depth + $this->nodeDepthSumSecondSolution($root->right, $depth + 1) + $this->nodeDepthSumSecondSolution($root->left, $depth + 1);
    }
}

class StackItem
{
    public ?Node $node;
    public int $depth;

    public function __construct($node, $depth)
    {
        $this->node = $node;
        $this->depth = $depth;
    }
}

class Stack
{
    private array $stack = [];

    /**
     * @param StackItem $data
     * @return void
     */
    public function push(StackItem $data): void
    {
        $this->stack[] = $data;
    }

    /**
     * @return mixed
     */
    public function pop(): mixed
    {
        return array_pop($this->stack);
    }

    public function isEmpty(): bool
    {
        return empty($this->stack);
    }

    public function count(): int
    {
        return count($this->stack);
    }
}

$tree = new BinarySearchTree();
//$tree->insert(1);
//$tree->insert(2);
//$tree->insert(3);
//$tree->insert(4);
//$tree->insert(5);
//$tree->insert(6);
//$tree->insert(7);
//$tree->insert(8);
//$tree->insert(9);
//$tree->insert(10);

$tree->insert('A');
$tree->insert('B');
$tree->insert('C');
$tree->insert('D');
$tree->insert('E');
$tree->insert('F');
$tree->insert('G');
$tree->insert('H');
$tree->insert('I');
$tree->insert('J');
$tree->insert('K');

$tree->balanceTree();

//echo '<pre>';
//echo $tree->nodeDepthSumFirstSolution($tree->root);
//echo $tree->nodeDepthSumSecondSolution($tree->root);
//var_dump($tree->branchSums($tree->root));
$tree->printTree($tree->root, 0);
//echo '<br>Closest value = ' .$tree->findClosestVal($tree, 14);