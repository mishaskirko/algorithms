<?php
declare(strict_types=1);

class GraphNode
{
    /**
     * @param ?string $value
     * @param array $neighbors
     */
    public function __construct(
        public ?string $value = null,
        public array $neighbors = []
    ) {}
}

class Graph
{
    /**
     * @param array $nodes
     */
    public function __construct(
        public array $nodes = []
    ) {}

    /**
     * @param GraphNode $node
     * @return void
     */
    public function addNode(GraphNode $node): void {
        $this->nodes[] = $node;
    }

    /**
     * @param GraphNode $from
     * @param GraphNode $to
     * @return void
     */
    public function addEdge(GraphNode $from, GraphNode $to): void
    {
        $from->neighbors[] = $to;
//        $to->neighbors[] = $from;
    }

    /**
     * @return array
     */
    public function getNodes(): array
    {
        return $this->nodes;
    }

    /**
     * @param GraphNode $node
     * @return array
     */
    public function getNeighbors(GraphNode $node): array
    {
        return $node->neighbors;
    }

    /**
     * @param $value
     * @return ?GraphNode
     */
    public function getNode($value): ?GraphNode
    {
        foreach ($this->nodes as $node) {
            if ($node->value === $value) {
                return $node;
            }
        }
        return null;
    }

    /**
     * @return void
     */
    public function printGraph(): void
    {
        foreach ($this->nodes as $node) {
            echo '<div style="font-weight: bold">' . $node->value . ':</div style="text">';
            foreach ($node->neighbors as $neighbor) {
                echo $neighbor->value . '<br/>';
            }
            echo PHP_EOL;
        }
    }

    public function depthFirstSearch(GraphNode $graphNode, &$array = []): array
    {
        $array[] = $graphNode->value;

        foreach ($graphNode->neighbors as $neighbor) {
            $this->depthFirstSearch($neighbor, $array);
        }

        return $array;
    }
}

$graph = new Graph();
$graph->addNode(new GraphNode('A'));
$graph->addNode(new GraphNode('B'));
$graph->addNode(new GraphNode('C'));
$graph->addNode(new GraphNode('D'));
$graph->addNode(new GraphNode('E'));
$graph->addNode(new GraphNode('F'));
$graph->addNode(new GraphNode('G'));
$graph->addNode(new GraphNode('H'));
$graph->addNode(new GraphNode('I'));
$graph->addNode(new GraphNode('J'));
$graph->addNode(new GraphNode('K'));

$graph->addEdge($graph->getNode('A'), $graph->getNode('B'));
$graph->addEdge($graph->getNode('A'), $graph->getNode('C'));
$graph->addEdge($graph->getNode('A'), $graph->getNode('D'));
$graph->addEdge($graph->getNode('B'), $graph->getNode('E'));
$graph->addEdge($graph->getNode('B'), $graph->getNode('F'));
$graph->addEdge($graph->getNode('F'), $graph->getNode('I'));
$graph->addEdge($graph->getNode('F'), $graph->getNode('J'));
$graph->addEdge($graph->getNode('D'), $graph->getNode('G'));
$graph->addEdge($graph->getNode('D'), $graph->getNode('H'));
$graph->addEdge($graph->getNode('J'), $graph->getNode('K'));

var_dump($graph->depthFirstSearch($graph->getNode('A')));

$graph->printGraph();