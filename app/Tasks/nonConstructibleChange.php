<?php
declare(strict_types=1);

class nonConstructibleChange
{
    private const COINS = [5, 7, 1, 1, 2, 3, 22];

    /**
     * O(nlog(n)) time complexity | O(1) space complexity
     */
    public function nonConstructibleChange(array $coins = self::COINS)
    {
        sort($coins);

        $currentChange = 0;

        for($i = 0; $i < count($coins); $i++) {
            if ($coins[$i] > $currentChange + 1) {
                return $currentChange + 1;
            }

            $currentChange += $coins[$i];
        }
        return $currentChange + 1;
    }
}

var_dump((new nonConstructibleChange())->nonConstructibleChange());