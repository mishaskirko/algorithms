<?php
declare(strict_types=1);

class TournamentWinner
{
    private const COMPETITIONS = [["HTML", "C#"], ["C#", "Python"], ["Python", "HTML"]];
    private const RESULTS = [0, 0, 1];
    private const HOME_TEAM_WON = 1;

    /**
     * O(n) time complexity | O(n) space complexity
     *
     * @param string[][] $competitions
     * @param int[] $results
     */
    public function tournamentWinner(
        array $competitions = self::COMPETITIONS,
        array $results = self::RESULTS
    ): string
    {
        $currentBestTeam = '';
        $scores = ["currentBestTeam" => 0];

        for ($i = 0; $i < count($competitions); $i++) {
            $result = $results[$i];
            [$homeTeam, $awayTeam] = $competitions[$i];

            $winningTeam = $result == self::HOME_TEAM_WON ? $homeTeam : $awayTeam;

            $scores = $this->updateScores($winningTeam, 3, $scores);

            if ($scores[$winningTeam] > $scores[$currentBestTeam]) {
                $currentBestTeam = $winningTeam;
            }
        }
        return $currentBestTeam;
    }

    public function updateScores($team, $points, $scores)
    {
        if (!array_key_exists($team, $scores)) {
            $scores[$team] = 0;
        }
        $scores[$team] += $points;

        return $scores;
    }
}

var_dump((new TournamentWinner())->tournamentWinner());