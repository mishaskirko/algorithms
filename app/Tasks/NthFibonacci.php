<?php
declare(strict_types=1);

class NthFibonacci
{
    /**
     * O(2^n) time complexity | O(n) space complexity
     */
    public function getNthFibonacci($n)
    {
        if ($n == 1) {
            return 0;
        }
        if ($n == 2) {
            return 1;
        }
        return $this->getNthFibonacci($n - 1) + $this->getNthFibonacci($n - 2);
    }

    /**
     * O(n) time complexity | O(n) space complexity
     */
    public function getNthFibonacciWithMemoize($n, array $memoize = [1 => 0, 2 => 1])
    {
        if (isset($memoize[$n])) {
            return $memoize[$n];
        }
        $memoize[$n] = $this->getNthFibonacciWithMemoize($n - 1, $memoize) + $this->getNthFibonacciWithMemoize($n - 2, $memoize);
        return $memoize[$n];
    }

    /**
     * O(n) time complexity | O(1) space complexity
     */
    public function getNthFibonacciWithDynamicProgramming($n) {
        $first = 0;
        $second = 1;
        for ($i = 3; $i <= $n; $i++) {
            $third = $first + $second;
            $first = $second;
            $second = $third;
        }
        return $second;
    }
}

$nthFibonacci = new NthFibonacci();
var_dump($nthFibonacci->getNthFibonacciWithDynamicProgramming(9));