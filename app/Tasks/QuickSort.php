<?php
declare(strict_types=1);

class QuickSort
{
    public const ARRAY = [1, 8, 43, 5, 7, 8, 33, 4, 5, 6];

    public function sort(array $array): array
    {
        if (count($array) < 2) {
            return $array;
        }
        $pivot = $array[0];
        $left = $right = [];
        for ($i = 1; $i < count($array); $i++) {
            if ($array[$i] < $pivot) {
                $left[] = $array[$i];
            } else {
                $right[] = $array[$i];
            }
        }
        return array_merge($this->sort($left), [$pivot], $this->sort($right));
    }
}