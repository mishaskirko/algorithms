<?php
declare(strict_types=1);

class InsertionSort
{

    /**
     * @param array $array
     * @return array
     */
    public function insertionSort(array $array): array
    {
        for ($i = 1; $i <= count($array) - 1; $i++) {
            $j = $i;
            while ($j > 0 && $array[$j] < $array[$j - 1]) {
                $this->swap($j, $j - 1, $array);
                $j -= 1;
            }
        }
        return $array;
    }

    /**
     * @param int $i
     * @param int $j
     * @param array $array
     * @return void
     */
    private function swap(int $i, int $j, array &$array): void
    {
        $temp = $array[$j];
        $array[$j] = $array[$i];
        $array[$i] = $temp;
    }
}

var_dump((new InsertionSort())->insertionSort([8, 5, 2, 9, 5, 6, 3]));