<?php
declare(strict_types=1);

class ThreeLargestNumbers
{
    /**
     * @param array $array
     * @return float
     */
    public function threeLargestNumber(array $array): float
    {
        $threeLargest = [null, null, null];
        $time = microtime(true);

        for ($i = 0; $i < count($array); $i++) {
            $this->updateLargest($threeLargest, $array[$i]);
        }

//        return $threeLargest;
        return microtime(true) - $time;
    }

    /**
     * @param array $threeLargest
     * @param int $number
     * @return void
     */
    private function updateLargest(array &$threeLargest, int $number): void
    {
        if ($threeLargest[2] === null || $number > $threeLargest[2]) {
            $this->shiftAndUpdate($threeLargest, $number, 2);
        } elseif ($threeLargest[1] === null || $number > $threeLargest[1]) {
            $this->shiftAndUpdate($threeLargest, $number, 1);
        } elseif ($threeLargest[0] === null || $number > $threeLargest[0]) {
            $this->shiftAndUpdate($threeLargest, $number, 0);
        }
    }

    /**
     * @param array $threeLargest
     * @param int $number
     * @param int $index
     * @return void
     */
    private function shiftAndUpdate(array &$threeLargest, int $number, int $index): void
    {
        for ($i = 0; $i <= $index; $i++) {
            if ($i === $index) {
                $threeLargest[$i] = $number;
            } else {
                $threeLargest[$i] = $threeLargest[$i + 1];
            }

        }
    }

    /**
     * @param array $array
     * @return array
     */
    public function threeLargestNumberSortSolution(array $array): array
    {
        rsort($array);

        return [$array[0], $array[1], $array[2]];
    }

    public function generateRandomArray(int $min, int $max, int $size): array
    {
        $array = [];
        for ($i = 0; $i < $size; $i++) {
            $array[] = rand($min, $max);
        }
        return $array;
    }
}

$threeLargestNumbers = new ThreeLargestNumbers();
$array = $threeLargestNumbers->generateRandomArray(-10000, 10000, 10000000);
//var_dump($array);

//O(N) time and O(1) space
//$result = $threeLargestNumbers->threeLargestNumber($array);

//O(NlogN) time and O(1) space
$result2 = $threeLargestNumbers->threeLargestNumberSortSolution($array);
echo 'TESTS ON 1000000 VALUES';
var_dump('WITHOUT SORTING(GAY METHOD):', 'WITH SORTING(GIGACHAD METHOD)' . $result2);