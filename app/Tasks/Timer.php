<?php
declare(strict_types=1);

class Timer
{
    /**
     * @param $microtime
     * @return string
     */
    public function getTimeElapsed($microtime): string
    {
        return number_format(microtime(true) - $microtime, 6);
    }
}