<?php
declare(strict_types=1);

class InterpolationSearch
{
    /**
     *
     *
     * @param array $arr
     * @param int $target
     * @return int
     */
    public function search(array $arr, int $target): int
    {
        $low = 0;
        $high = count($arr) - 1;
        while ($low <= $high) {
            $mid = $low + ($high - $low) * ($target - $arr[$low]) / ($arr[$high] - $arr[$low]);
            if ($arr[$mid] < $target) {
                $low = $mid + 1;
            } elseif ($arr[$mid] > $target) {
                $high = $mid - 1;
            } else {
                return $mid;
            }
        }
        return -1;
    }
}

$arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$key = 5;
$search = new InterpolationSearch();
$result = $search->search($arr, $key);