<?php
declare(strict_types=1);

class ValidateSubsequence {

    private const ARRAY = [5, 1, 22, 25, 6, -1, 8, 10];
    private const SUB_SEQUENCE = [1, 6, -1, 10];

    /**
     * O(nlog(n)) time complexity | O(n) space complexity
     *
     * @param array $sequence
     * @param array $subSequence
     * @return string
     */
    public function firstSolution(array $sequence = self::ARRAY, array $subSequence = self::SUB_SEQUENCE): string
    {
        $seqIdx = 0;
        $subSeqIdx = 0;
        while($seqIdx < count($sequence) && $subSeqIdx < $subSequence) {
            if ($sequence[$seqIdx] == $subSequence[$subSeqIdx]) {
                $subSeqIdx += 1;
            }
            $seqIdx += 1;
        }
        return microtime();
//        return $subSeqIdx == count($subSequence);
    }

    /**
     * O(n) time complexity | O(n) space complexity
     *
     * @param array $sequence
     * @param array $subSequence
     * @return string
     */
    public function secondSolution(array $sequence = self::ARRAY, array $subSequence = self::SUB_SEQUENCE): string
    {
        $subSeqIdx = 0;
        for ($i = 0; $i < count($sequence); $i++) {
            if ($subSeqIdx == count($subSequence)) {
                break;
            }
            if ($subSequence[$subSeqIdx] == $sequence[$i]) {
                $subSeqIdx += 1;
            }
        }
        return microtime();
//        return $subSeqIdx == count($subSequence);
    }
}

var_dump((new ValidateSubsequence())->firstSolution());
var_dump((new ValidateSubsequence())->secondSolution());