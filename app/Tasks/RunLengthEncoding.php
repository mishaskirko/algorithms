<?php
declare(strict_types=1);

class RunLengthEncoding
{

    public function encode(string $input): string
    {
        $encodedStringCharacters = [];
        $currentRunLength = 1;

        for ($i = 1; $i <= strlen($input); $i++) {
            $currentCharacter = $input[$i - 1];
            $nextCharacter = $input[$i] ?? null;

            if ($currentCharacter !== $nextCharacter) {
                $encodedStringCharacters[] = $currentRunLength > 1 ? $currentRunLength : '';
                $encodedStringCharacters[] = $currentCharacter;
                $currentRunLength = 1;
            } else {
                $currentRunLength++;
            }
        }

        return implode('', $encodedStringCharacters);
    }

    public function decode(string $input): string
    {
        $decodedStringCharacters = [];
        $currentRunLength = '';

        for ($i = 0; $i < strlen($input); $i++) {
            $currentCharacter = $input[$i];

            if (is_numeric($currentCharacter)) {
                $currentRunLength .= $currentCharacter;
            } else {
                $decodedStringCharacters[] = str_repeat($currentCharacter, $currentRunLength ?: 1);
                $currentRunLength = '';
            }
        }

        return implode('', $decodedStringCharacters);
    }
}

var_dump((new RunLengthEncoding())->encode('AAAAAAAAAAAA222AAAAAAAAAABBBCCDAA'));
var_dump((new RunLengthEncoding())->decode('12A3210A3B2CD2A'));