<?php
declare(strict_types=1);

class SumOfTwoNumbers {

    private const ARRAY = [3, 5, -4, 8, 11, 1, -1, 6];
    private const TARGET_NUM = 10;

    /**
     * O(n^2) time complexity | O(1) space complexity
     *
     * @param array $array
     * @param int $targetNum
     * @return array|string
     */
    public function firstSolution(array $array = self::ARRAY, int $targetNum = self::TARGET_NUM): array|string
    {
        for ($i = 0; $i <= count($array) - 1; $i++) {
            $firstNum = $array[$i];
            for ($j = $i + 1; $j <= count($array); $j++) {
                $secondNum = $array[$j];
                if ($firstNum + $secondNum == $targetNum) {
                   return microtime();
//                    return [$firstNum, $secondNum];
                }
            }
        }
        return [];
    }

    /**
     * O(n) time complexity | O(n) space complexity
     * @param array $array
     * @param int $targetNum
     * @return array|string
     */
    public function secondSolution(array $array = self::ARRAY, int $targetNum = self::TARGET_NUM): array|string
    {
        $nums = [];
        for ($i = 0; $i <= count($array); $i++) {
            $potentialMatch = $targetNum - $array[$i];
            if (in_array($potentialMatch, $nums)) {
                return microtime();
//                return [$potentialMatch, $array[$i]];
            } else {
                $nums[] = $array[$i];
            }
        }
        return [];
    }

    /**
     * O(nlog(n)) time complexity | O(1) space complexity
     *
     * @param array $array
     * @param int $targetNum
     * @return array|string
     */
    public function thirdSolution(array $array = self::ARRAY, int $targetNum = self::TARGET_NUM): array|string
    {
        sort($array);
        $left = 0;
        $right = count($array) -1;
        while ($left < $right) {
            $currentSum = $array[$left] + $array[$right];
            if ($currentSum == $targetNum) {
                return microtime();
//                return [$array[$left], $array[$right]];
            } elseif ($currentSum < $targetNum) {
                $left += 1;
            } elseif ($currentSum > $targetNum) {
                $right -= 1;
            }
        }
        return [];
    }
}

var_dump((new SumOfTwoNumbers())->firstSolution(range(-1000, 1000), 32));
var_dump((new SumOfTwoNumbers())->secondSolution(range(-1000, 1000), 32));
var_dump((new SumOfTwoNumbers())->thirdSolution(range(-1000, 1000), 32));