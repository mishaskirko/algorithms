<?php

class BubleSort
{
    public const ARRAY = [1 ,4, 5, 7, 8, 9, 10, 33, 22, 5, 6, 4, 3];

    public function sort($array = self::ARRAY)
    {
        $count = count($array);
        for ($i = 0; $i < $count; $i++) {
            for ($j = 0; $j < $count - 1; $j++) {
                if ($array[$j] > $array[$j + 1]) {
                    $temp = $array[$j];
                    $array[$j] = $array[$j + 1];
                    $array[$j + 1] = $temp;
                }
            }
        }
        return $array;
    }
}

$sort = new BubleSort();
var_dump($sort->sort());