<?php
declare(strict_types=1);

class ProductSum
{
    public const ARRAY = [5, 2, [7, -1], 3, [6, [-13, 8], 4]];

    public function productSum(array $array = self::ARRAY, int $multiplier = 1): int
    {
        $sum = 0;
        for ($i = 0; $i < count($array); $i++) {
            if (is_array($array[$i])) {
                $sum += $this->productSum($array[$i], $multiplier + 1);
            } else {
                $sum += $array[$i];
            }
        }
        return $sum * $multiplier;
    }
}

$productSum = new ProductSum();
var_dump($productSum->productSum());