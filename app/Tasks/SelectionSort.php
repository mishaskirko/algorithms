<?php
declare(strict_types=1);

class SelectionSort
{
    public function sort(array $array): array
    {
        $length = count($array);
        for ($i = 0; $i < $length; $i++) {
            $min = $i;
            for ($j = $i + 1; $j < $length; $j++) {
                if ($array[$j] < $array[$min]) {
                    $min = $j;
                }
            }
            $temp = $array[$i];
            $array[$i] = $array[$min];
            $array[$min] = $temp;
        }
        return $array;
    }
}