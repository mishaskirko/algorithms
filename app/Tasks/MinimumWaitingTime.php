<?php
declare(strict_types=1);

class MinimumWaitingTime
{
    const QUERIES = [3, 2, 1, 2, 6];

    public function calculateMinimumWaitingTimeFor($queries = self::QUERIES)
    {
        sort($queries);
        $totalWaitingTime = 0;
        $start = microtime(true);
        for ($i = 0; $i <= count($queries) - 1; $i++) {
            $queriesLeft = count($queries) - ($i + 1);
            $totalWaitingTime += $queries[$i] * $queriesLeft;
        }
        return microtime(true) - $start;
    }

    public function calculateMinimumWaitingTimeForeach($queries = self::QUERIES)
    {
        sort($queries);
        $totalWaitingTime = 0;
        $start = microtime(true);
        foreach ($queries as $key => $value) {
            $queriesLeft = count($queries) - ($key + 1);
            $totalWaitingTime += $value * $queriesLeft;
        }

        return microtime(true) - $start;
    }
}

var_dump((new MinimumWaitingTime())->calculateMinimumWaitingTimeForeach());
var_dump((new MinimumWaitingTime())->calculateMinimumWaitingTimeFor());