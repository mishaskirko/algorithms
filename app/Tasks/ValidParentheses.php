<?php
declare(strict_types=1);

class ValidParentheses
{
    /**
     * O(nlog(n)) time complexity | O(n^2) space complexity
     */
    function isValid($s): bool
    {
        $stack = [];
        $map = [
            '(' => ')',
            '{' => '}',
            '[' => ']',
        ];
        for ($i = 0; $i < strlen($s); $i++) {
            $char = $s[$i];
            if (array_key_exists($char, $map)) {
                $stack[] = $char;
            } else {
                if (count($stack) == 0) {
                    return false;
                }
                $last = array_pop($stack);
                if ($map[$last] != $char) {
                    return false;
                }
            }
        }
        return count($stack) == 0;
    }

    public function revertString($s): string
    {
        $result = '';
        for ($i = strlen($s) - 1; $i >= 0; $i--) {
            $result .= $s[$i];
        }
        return $result;
    }
}



var_dump((new ValidParentheses())->isValid("{[]}"));