<?php
declare(strict_types=1);

class LinkedListNode
{
    /**
     * @param int $data
     * @param LinkedListNode|null $next
     */
    public function __construct(
        public int $data,
        public ?LinkedListNode $next = null
    ){}
}

class LinkedList
{
    /**
     * @param int $count
     * @param LinkedListNode|null $head
     * @param LinkedListNode|null $tail
     */
    public function __construct(
        private ?LinkedListNode $head = null,
        private ?LinkedListNode $tail = null,
        private int $count = 0,
    ){}

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $data
     * @return void
     */
    public function add(int $data): void
    {
        $node = new LinkedListNode($data);
        if ($this->head === null) {
            $this->head = $node;
        } else {
            $this->tail->next = $node;
        }
        $this->tail = $node;
        $this->count++;
    }

    /**
     * @param int $data
     * @return void
     */
    public function remove(int $data): void
    {
        if ($this->head === null) {
            return;
        }
        if ($this->head->data === $data) {
            $this->head = $this->head->next;
            $this->count--;
            return;
        }
        $prev = $this->head;
        $curr = $this->head->next;
        while ($curr !== null) {
            if ($curr->data === $data) {
                $prev->next = $curr->next;
                $this->count--;
                return;
            }
            $prev = $curr;
            $curr = $curr->next;
        }
    }

    /**
     * @param LinkedList $linkedList
     * @return LinkedList
     */
    public function removeDuplicates(LinkedList $linkedList): LinkedList
    {
        $curr = $linkedList->head;
        while ($curr !== null) {
            $next = $curr->next;
            while ($next !== null) {
                if ($curr->data === $next->data) {
                    $curr->next = $next->next;
                    $this->count--;
                }
                $next = $next->next;
            }
            $curr = $curr->next;
        }
        return $linkedList;
    }

    /**
     * @return void
     */
    public function print(): void
    {
        $curr = $this->head;
        while ($curr !== null) {
            if ($this->head->data === $curr->data) {
                echo '(HEAD) ' . $curr->data . ' => ';
            } elseif ($this->tail->data === $curr->data) {
                echo '(TAIL) ' . $curr->data . ' => ' . 'NULL';
            } else {
                echo $curr->data . ' => ';
            }
            $curr = $curr->next;
        }
        echo PHP_EOL;
    }
}

$linkedList = new LinkedList();
$linkedList->add(1);
$linkedList->add(1);
$linkedList->add(3);
$linkedList->add(4);
$linkedList->add(4);
$linkedList->add(5);
$linkedList->add(6);
$linkedList->add(6);

$linkedList->removeDuplicates($linkedList);
$linkedList->print();