<?php
declare(strict_types=1);

class PalindromeCheck
{

    /**
     * O(n^2 ) time | O(n) space
     *
     * @param string $string
     * @return bool
     */
    public function isPalindrome(string $string): bool
    {
        $reversedString = '';
        for ($i = strlen($string) - 1; $i >= 0; $i--) {
            $reversedString .= $string[$i];
        }
        return $string === $reversedString;
    }
}