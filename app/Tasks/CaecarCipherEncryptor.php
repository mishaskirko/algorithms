<?php
declare(strict_types=1);

class CaecarCipherEncryptor
{

    /**
     * O(n) time | O(n) space
     *
     * @param string $string
     * @param int $key
     * @return string
     */
    public function caesarCypherEncryptor(string $string, int $key): string
    {
        $newLetters = [];
        $newKey = $key % 26;
        $alphabet = range('a', 'z');
        for ($i = 0; $i < strlen($string); $i++) {
            $newLetters[] = $this->getNewLetter($string[$i], $newKey, $alphabet);
        }
        return implode('', $newLetters);
    }

    /**
     * @param string $letter
     * @param int $key
     * @param array $alphabet
     * @return string
     */
    private function getNewLetter(string $letter, int $key, array $alphabet): string
    {
        $newLetterCode = array_search($letter, $alphabet) + $key;
        return $alphabet[$newLetterCode % 26];
    }
}