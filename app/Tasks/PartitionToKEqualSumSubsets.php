<?php
declare(strict_types=1);

class Solution {

    public const NUMS = [4,4,6,2,3,8,10,2,10,7];
    public const K = 4;

    /**
     * @param Integer[] $nums
     * @param Integer $k
     * @return Boolean
     */
    function canPartitionKSubsets(array $nums = self::NUMS, int $k = self::K) {
        if(array_sum($nums) % $k != 0) {
            return false;
        }

    }
}

var_dump((new Solution())->canPartitionKSubsets());