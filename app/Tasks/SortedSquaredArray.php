<?php
declare(strict_types=1);

class SortedSquaredArray
{
    private const ARRAY = [-7, -5, -4, 3, 6, 8, 9];

    public function __construct(
        private readonly Timer $timer,
    ) {}

    /**
     * O(nlog(n)) time complexity | O(n) space complexity
     *
     * @param array $array
     * @return string
     */
    public function firstSolution(array $array = self::ARRAY): string
    {
        $microtime = microtime(true);
        $squares = array_fill(0, count($array), 0);

        for ($i = 0; $i < count($array); $i++) {
            $value = $array[$i];
            $squares[$i] = $value ** 2;
        }
        sort($squares);

        return $this->timer->getTimeElapsed($microtime);
    }

    /**
     * O(n) time complexity | O(n) space complexity
     *
     * @param array $array
     * @return string
     */
    public function secondSolution(array $array = self::ARRAY): string
    {
        $microtime = microtime(true);
        $squares = array_fill(0, count($array), 0);
        $smallerValueIdx = 0;
        $largerValueIdx = count($array) - 1;

        for ($i = count($array) - 1; $i >= 0; $i--) {
            $smallerValue = $array[$smallerValueIdx];
            $largerValue = $array[$largerValueIdx];

            if (abs($smallerValue) > abs($largerValue)) {
                $squares[$i] = $smallerValue ** 2;
                $smallerValueIdx += 1;
            } else {
                $squares[$i] = $largerValue ** 2;
                $largerValueIdx -= 1;
            }
        }
        return $this->timer->getTimeElapsed($microtime);
    }
}