<?php
declare(strict_types=1);

class TandemBicycle
{
    const RED_SHIRT_SPEEDS = [5, 5, 3, 9, 2];
    const BLUE_SHIRT_SPEEDS = [3, 6, 7, 2, 1];
    const FASTEST = false;

    public function tandemBicycle(
        $redShirtSpeed = self::RED_SHIRT_SPEEDS,
        $blueShirtSpeed = self::BLUE_SHIRT_SPEEDS,
        $fastest = self::FASTEST)
    {
        sort($redShirtSpeed);
        sort($blueShirtSpeed);

        if (!$fastest) {
            $blueShirtSpeed = array_reverse($blueShirtSpeed);
        }

        $totalSpeed = 0;
        for ($i = 0; $i < count($blueShirtSpeed); $i++) {
            $rider1 = $blueShirtSpeed[$i];
            $rider2 = $redShirtSpeed[count($redShirtSpeed) - $i - 1];
            $totalSpeed += max($rider1, $rider2);
        }
        return $totalSpeed;
    }
}

$tandemBicycle = new TandemBicycle();
var_dump($tandemBicycle->tandemBicycle());