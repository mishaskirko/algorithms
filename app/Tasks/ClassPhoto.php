<?php
declare(strict_types=1);

class ClassPhoto
{
    public const RED_SHIRT_HEIGHTS = [9, 8, 1, 3, 4];
    public const BLUE_SHIRT_HEIGHTS = [6, 8, 0, 2, 3];

    /**
     * @param array $redShirtHeights
     * @param array $blueShirtHeights
     * @return bool
     */
    public function classPhotos(
        array $redShirtHeights = self::RED_SHIRT_HEIGHTS,
        array $blueShirtHeights = self::BLUE_SHIRT_HEIGHTS
    ): bool
    {
        sort($redShirtHeights, SORT_DESC);
        sort($blueShirtHeights, SORT_DESC);

        $shirtColorInFirstRow = $redShirtHeights[0] > $blueShirtHeights[0] ? 'red' : 'blue';

        for ($i = 0; $i <= count($redShirtHeights) - 1; $i++) {
            switch ($shirtColorInFirstRow) {
                case 'red':
                    if ($redShirtHeights[$i] <= $blueShirtHeights[$i]) {
                        return false;
                    }
                    break;
                case 'blue':
                    if ($blueShirtHeights[$i] <= $redShirtHeights[$i]) {
                        return false;
                    }
                    break;
            }
        }
        return true;
    }
}

var_dump((new ClassPhoto())->classPhotos());