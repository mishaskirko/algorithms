<?php
declare(strict_types=1);

class BinarySearch
{
    /**
     * O(log(n)) time complexity | O(1) space complexity
     *
     * @param $array
     * @param $key
     * @return float|int
     */
    public function iterativeSearch($array, $key): float|int
    {
        $low = 0;
        $high = count($array) - 1;
        while ($low <= $high) {
            $mid = floor(($low + $high) / 2);
            if ($array[$mid] == $key) {
                return $mid;
            } else if ($array[$mid] < $key) {
                $low = $mid + 1;
            } else {
                $high = $mid - 1;
            }
        }
        return -1;
    }

    /**
     * O(log(n)) time complexity | O(log(n)) space complexity
     * @param $array
     * @param $key
     * @param $low
     * @param $high
     * @return float|int
     */
    public function recursiveSearch($array, $key, $low, $high): float|int
    {
        if ($low > $high) {
            return -1;
        }
        $mid = floor(($low + $high) / 2);
        if ($array[$mid] == $key) {
            return $mid;
        } else if ($array[$mid] < $key) {
            return $this->recursiveSearch($array, $key, $mid + 1, $high);
        } else {
            return $this->recursiveSearch($array, $key, $low, $mid - 1);
        }
    }
}

$binarySearch = new BinarySearch();
$array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$key = 10;
$result = $binarySearch->iterativeSearch($array, $key);
var_dump($result);
